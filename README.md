# Build custom live distro from scratch

Based on https://nathanpfry.com/how-to-customize-an-ubuntu-installation-disc/

 :exclamation: Right now Gitlab does not allow to upload a file about 1G in size (for good reasons ;) ). But this works in my private Gitlab there limits have been raised. So just fork this into your instance ;) 

## Customize

```setup.sh``` will be used to setup (packages, theming, ...) the chrooted target. Just change it to satisfy your needs.

## PXE

Download ```vmlinuz-x.y.z-x-generic``` and ```initrd.img-x.y.z-x-generic``` from artifacts into a directory on your pxe server and make it available via tftp like this:

```
# EXAMPLE
label custom-live-16.04
        menu label Our fancy live image
        kernel custom/vmlinuz-x.y.z-x-generic
        append boot=casper netboot=nfs nfsroot=$NFS_IP:/$CUSTOM_DIR locale=de_DE.UTF-8 console-keymaps-at/keymap=de netcfg/get_hostname=live vga=normal initrd=custom/initrd.img-x.y.z-x-generic quiet splash persistent --
```

## NFS

Download ```my-live-pxe.tgz```, unpack into a directory and export it:

```
cd $CUSTOM_DIR # or whatever your directory is
tar xzf my-live-pxe.tgz
cat >> /etc/exports < EOF
$CUSTOM_DIR $IP_OR_NETWORK(insecure,ro,no_root_squash,no_subtree_check) # adjust to your needs
EOF
exportfs -arv
```

## ISO

You can download the pxe archive, extract, enter the directory and then run:

```
genisoimage -D -r -V "Custom ISO" -cache-inodes -J -l -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -o ../my-live.iso .
```

If you only want to have the ISO and no PXE boot files (you can extract them from the ISO later anyway), then just replace

```
  - tar czf ../my-live-pxe.tgz .
  artifacts:
    paths:
    - $CI_PROJECT_DIR/my-live-pxe.tgz
    - $CI_PROJECT_DIR/edit/boot/vmlinuz-*
    - $CI_PROJECT_DIR/edit/boot/initrd.img-*
```

by

```
  - genisoimage -D -r -V "Custom ISO" -cache-inodes -J -l -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -o ../my-live.iso .
  artifacts:
    paths:
    - $CI_PROJECT_DIR/my-live.iso
```

inside ```.gitlab-ci.yml```.
