#!/bin/bash

################################################################################
## init
################################################################################

mount -t proc none /proc
mount -t sysfs none /sys
mount -t devpts none /dev/pts
export HOME=/root
export LC_ALL=C
dbus-uuidgen > /var/lib/dbus/machine-id
dpkg-divert --local --rename --add /sbin/initctl
ln -s /bin/true /sbin/initctl

################################################################################
## packages
################################################################################

apt-get update
apt-get dist-upgrade -y
apt-get install software-properties-common -y
add-apt-repository ppa:remmina-ppa-team/remmina-next -y
add-apt-repository -y ppa:embrosyn/cinnamon
add-apt-repository -y ppa:numix/ppa
add-apt-repository -y ppa:libreoffice/ppa
echo 'deb http://download.opensuse.org/repositories/home:/Horst3180/xUbuntu_16.04/ /' > /etc/apt/sources.list.d/arc-theme.list
wget -q http://download.opensuse.org/repositories/home:Horst3180/xUbuntu_16.04/Release.key -O - | apt-key add -
add-apt-repository -y ppa:moka/stable
apt-add-repository multiverse
apt-get update
apt-get -y install --install-recommends linux-generic-hwe-16.04 xserver-xorg-hwe-16.04
apt-get -y install \
	adwaita-icon-theme-full \
	arc-theme \
	bash-completion \
	blueman \
	chromium-browser \
	cinnamon \
	cinnamon-session \
	curl \
	command-not-found \
	dconf-tools \
	faba-icon-theme \
	firefox \
	firefox-locale-de \
	firefox-locale-en \
	fonts-noto \
	gedit \
	gedit-plugins \
	git \
	gnupg2 \
	gnome-terminal \
	gnome-calculator \
	hunspell-de-at \
	hunspell-de-ch \
	hunspell-de-de \
	hunspell-en-ca \
	hyphen-de \
	hyphen-en-us \
	language-selector-gnome \
	libreoffice \
	libreoffice-help-de \
	libreoffice-help-en-gb \
	libreoffice-l10n-de \
	libreoffice-l10n-en-gb \
	libreoffice-l10n-en-za \
	lightdm \
	lightdm-gtk-greeter \
	light-themes \
	mc \
	myspell-en-au \
	myspell-en-gb \
	myspell-en-za \
	mythes-de \
	mythes-de-ch \
	mythes-en-au \
	mythes-en-us \
	moka-icon-theme \
	nemo \
	nemo-fileroller \
	nemo-gtkhash \
	nemo-image-converter \
	nemo-media-columns \
	nemo-preview \
	nemo-seahorse \
	nemo-share \
	openoffice.org-hyphenation \
	pk-update-icon \
	plymouth-theme-ubuntu-logo \
	pm-utils \
	poppler-data \
	rfkill \
	remmina \
	remmina-plugin-rdp \
	ttf-ubuntu-font-family \
	ubuntu-drivers-common \
	ubuntu-wallpapers \
	vim wamerican \
	wbritish \
	wngerman \
	wogerman \
	wswiss \
	xdg-user-dirs \
	xinit

################################################################################
## fix network-manager
################################################################################

logger -t late-command "fixing network-manager"

cat > /etc/network/interfaces << EOF
# interfaces(5) file used by ifup(8) and ifdown(8)
auto lo
iface lo inet loopback

EOF

################################################################################
## fix shutdown menu
################################################################################

logger -t late-command "fixing shutdown menu"

cat > /usr/share/glib-2.0/schemas/org.cinnamon.desktop.session.gschema.override << EOF
[org.cinnamon.desktop.session]
settings-daemon-uses-logind=true
session-manager-uses-logind=true
screensaver-uses-logind=false
EOF

glib-compile-schemas /usr/share/glib-2.0/schemas

################################################################################
## set cinnamon as default session
################################################################################

logger -t late-command "make cinnamon default"

cat > /usr/share/lightdm/lightdm.conf.d/50-cinnamon.conf << EOF
[SeatDefaults]
user-session=cinnamon
EOF

################################################################################
## set nemo as default filebrowser
################################################################################

logger -t late-command "make nemo default"

mkdir -p /etc/skel/.local/share/applications
cat > /etc/skel/.local/share/applications/mimeapps.list << EOF

[Default Applications]
inode/directory=nemo.desktop
application/x-gnome-saved-search=nemo.desktop
EOF

logger -t late-command "remove nautilus"

apt-get purge nautilus -y

################################################################################
## dconf settings
################################################################################

# enable custom presets

mkdir -p /etc/dconf/profile/
cat > /etc/dconf/profile/user << EOF
user-db:user
system-db:local
EOF

# fonts

mkdir -p /etc/dconf/db/local.d
cat > /etc/dconf/db/local.d/01-fonts << EOF
[org/cinnamon/desktop/interface]
font-name='Noto Sans 9'

[org/cinnamon/desktop/wm/preferences]
titlebar-font='Noto Sans Bold 10'

[org/gnome/desktop/interface]
document-font-name='Noto Sans 10'

[org/nemo/desktop]
font='Noto Sans 10'
EOF

# trayicon for language

cat > /etc/dconf/db/local.d/01-ibus-indicator << EOF
[desktop/ibus/panel]
show-icon-on-systray=false

[org/gnome/libgnomekbd/keyboard]
layouts=['de', 'us\talt-intl']
EOF

# theming

cat > /etc/dconf/db/local.d/01-theme << EOF
[org/cinnamon/theme]
name='Arc'

[org/cinnamon/desktop/interface]
icon-theme='Moka'
gtk-theme='Arc'

[org/cinnamon/desktop/background]
picture-uri='file:///usr/share/backgrounds/warty-final-ubuntu.png'
picture-options='zoom'

[org/cinnamon/desktop/slideshow]
image-source='directory:///usr/share/backgrounds'

[org/cinnamon/desktop/wm/preferences]
theme='Arc'

EOF

# finally apply

dconf update

################################################################################
## tweaks
################################################################################

## bashrc

sed -i 's,^#force_color_prompt,force_color_prompt,' /etc/skel/.bashrc
sed 's,01;32m,01;31m,' /etc/skel/.bashrc > /root/.bashrc

## lightdm-gtk-greeter

cat > /etc/lightdm/lightdm-gtk-greeter.conf << EOF
[greeter]
background = /usr/share/backgrounds/warty-final-ubuntu.png
theme-name = Arc
icon-theme-name = Moka
font-name = Noto Sans 11
EOF

################################################################################
## cleanup
################################################################################

logger -t late-command "cleanup"

apt-get purge avahi-* -y
apt-get autoremove -y
apt-get clean

rm /var/lib/dbus/machine-id
rm /sbin/initctl
dpkg-divert --rename --remove /sbin/initctl
umount /proc || umount -lf /proc
umount /sys
umount /dev/pts
